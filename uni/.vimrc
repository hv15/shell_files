" VIM Configure

" get line numbers 
set number

" don't wrap lines
set nowrap

" Enable filetype Plugins
filetype plugin on
filetype indent on

" active auto-load of edited files
set autoread

" when vimrc is edited, load immediately
autocmd! bufwritepost vimrc source ~/.vimrc

" always have ruler/position
set ruler

" case insensitive search
set ignorecase
set smartcase
set magic " regular expressions

" readability
set showmatch
set mat=2 "blinks every tenth of second

syntax enable " Enable syntax highlighting
set background=dark " ensure that commenting is visible
set gfn=Monospace\ 10
set shell=/bin/bash

colorscheme zellner
set background=dark

set ffs=unix,dos,mac "default file encoding

" tabs
set shiftwidth=4
set tabstop=4
set smarttab

set lbr "read left to right
set ai "auto indent
set si "smart indent

" Key Mapping
map <ESC>[C <C-Right>
map <ESC>[D <C-Left>
